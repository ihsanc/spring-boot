package com.ops.springapp.team;

import com.ops.springapp.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    TeamRepository teamRepository;

    public void setTeamRepository(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    public List<Team> getList() {
        return (List<Team>) teamRepository.findAll();

    }

    public Team getById(String id) {
        return teamRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Team.class, id));

    }

    public Team save(Team team) {
        return teamRepository.save(team);
    }

    public void deleteById(String id) {
        teamRepository.deleteById(id);
    }
}
