package com.ops.springapp.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teams")
public class TeamRestController {

    @Autowired
    private TeamService teamService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Team> listTeam() {
        return teamService.getList();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Team addNewTeam(@RequestBody Team team) {
        return teamService.save(team);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Team updateTeam(@PathVariable("id") String id) {
        return teamService.getById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteTeam(@PathVariable("id") String id) {
        teamService.deleteById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Team getTeam(@RequestBody Team team) {
        return teamService.save(team);
    }

}
