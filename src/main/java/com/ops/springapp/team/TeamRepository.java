package com.ops.springapp.team;

import com.ops.springapp.common.MyRepository;

public interface TeamRepository extends MyRepository<Team, String> {

}
