package com.ops.springapp.team;

import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ihsanc
 * @date 19.07.2018
 */
@Repository
public class InMemoryTeamRepository implements TeamRepository {

    Set<Team> teamList = new HashSet<>();

    @PostConstruct
    public void setUp() {
        teamList = Arrays.asList(
                new Team(UUID.randomUUID().toString(), "team1"),
                new Team(UUID.randomUUID().toString(), "team2")).stream().collect(Collectors.toSet());
    }

    @Override
    public List<Team> findAll() {
        return teamList.stream().collect(Collectors.toList());
    }

    @Override
    public Optional<Team> findById(String id) {
        return teamList.stream().
                filter(team -> team.getId().equals(id)).
                findFirst();
    }

    @Override
    public Team save(Team team) {
        teamList.add(team);
        return team;
    }

    @Override
    public void deleteById(String id) {
        teamList.removeIf(team -> team.getId().equals(id));
    }
}
