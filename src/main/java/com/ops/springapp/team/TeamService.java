package com.ops.springapp.team;

import java.util.List;

/**
 * @author ihsanc
 * @date 19.07.2018
 */
public interface TeamService {
    List<Team> getList();

    Team getById(String id);

    Team save(Team team);

    void deleteById(String id);
}
