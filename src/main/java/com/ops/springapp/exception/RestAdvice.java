package com.ops.springapp.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Calendar;
import java.util.Date;

/**
 * @author ihsanc
 * @date 18.07.2018
 */
@RestControllerAdvice
public class RestAdvice {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error handleEntityNotFoundException(EntityNotFoundException ex) {
        logger.error(ex.getMessage(), ex);
        return new Error(Calendar.getInstance().getTime(), ex.getMessage());
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error handleInternalServerErrors(Throwable ex) {
        logger.error(ex.getMessage(), ex);

        return new Error(new Date(), ex.getMessage());
    }

    public class Error {
        private Date time;
        private String message;

        public Error(Date time, String message) {
            this.time = time;
            this.message = message;
        }

        public Date getTime() {
            return time;
        }

        public String getMessage() {
            return message;
        }
    }
}
