package com.ops.springapp.exception;

/**
 * @author ihsanc
 * @date 18.07.2018
 */
public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(Class clz, String id) {
        super(String.format("Could not found %s with id: %s", clz.getSimpleName(), id));
    }
}
