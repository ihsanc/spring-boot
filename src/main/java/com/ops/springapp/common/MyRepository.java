package com.ops.springapp.common;

import java.util.List;
import java.util.Optional;

/**
 * @author ihsanc
 * @date 19.07.2018
 */
public interface MyRepository<T, I> {

    List<T> findAll();

    Optional<T> findById(I id);

    T save(T entity);

    void deleteById(I id);
}
