package com.ops.springapp.user;

import com.ops.springapp.common.MyRepository;

public interface UserRepository extends MyRepository<User, String> {

}
