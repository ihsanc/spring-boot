package com.ops.springapp.user;

import java.util.List;

/**
 * @author ihsanc
 * @date 19.07.2018
 */
public interface UserService {
    List<User> getList();

    User save(User u);

    User getById(String id);

    void deleteById(String id);
}
