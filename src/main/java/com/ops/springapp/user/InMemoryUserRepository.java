package com.ops.springapp.user;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ihsanc
 * @date 19.07.2018
 */
public class InMemoryUserRepository implements UserRepository {
    Set<User> userList = new HashSet<>();

    @PostConstruct
    public void setUp() {
        userList = Arrays.asList(
                new User(UUID.randomUUID().toString(), "user1"),
                new User(UUID.randomUUID().toString(), "user2")).stream().collect(Collectors.toSet());
    }

    @Override
    public List<User> findAll() {
        return userList.stream().collect(Collectors.toList());
    }

    @Override
    public Optional<User> findById(String id) {
        return userList.stream()
                .filter(user -> user.getId().equals(id))
                .findFirst();
    }

    @Override
    public User save(User user) {
        userList.add(user);
        return user;
    }

    @Override
    public void deleteById(String id) {
        userList.removeIf(user -> user.getId().equals(id));
    }
}
