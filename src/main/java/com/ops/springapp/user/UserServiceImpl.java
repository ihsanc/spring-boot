package com.ops.springapp.user;

import com.ops.springapp.exception.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    UserRepository userRepository;

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getList() {
        return (List<User>) userRepository.findAll();
    }

    public User save(User u) {
        logger.debug("User {} saved.", u.getId());
        return userRepository.save(u);
    }

    public User getById(String id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(User.class, id));
    }

    public void deleteById(String id) {
        logger.debug("User {} deleted.", id);
        userRepository.deleteById(id);
    }
}
