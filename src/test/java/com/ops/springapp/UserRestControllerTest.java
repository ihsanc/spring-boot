package com.ops.springapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ops.springapp.exception.EntityNotFoundException;
import com.ops.springapp.user.User;
import com.ops.springapp.user.UserRestController;
import com.ops.springapp.user.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Collections;
import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author ihsanc
 * @date 18.07.2018
 */
@RunWith(SpringRunner.class)
@WebMvcTest(UserRestController.class)
public class UserRestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserServiceImpl userService;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void listUserTest() throws Exception {
        when(userService.getList()).thenReturn(Collections.<User>emptyList());

        mockMvc.perform(get("/users"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));
    }

    @Test
    public void getUserTest() throws Exception {
        User t = new User(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        when(userService.getById(t.getId())).thenReturn(t);

        MvcResult result = mockMvc.perform(get("/users/{id}", t.getId()))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        assertThat(mapper.readValue(result.getResponse().getContentAsString(), User.class).getName(), is(equalTo(t.getName())));

        // Exception case
        when(userService.getById(t.getId())).thenThrow(new EntityNotFoundException(User.class, t.getId()));
        mockMvc.perform(get("/users/{id}", t.getId()))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void addUserTest() throws Exception {
        User t = new User(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        when(userService.save(any(User.class))).thenReturn(t);

        MvcResult result = mockMvc.perform(post("/users")
                .content(mapper.writeValueAsString(t))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        assertThat(mapper.readValue(result.getResponse().getContentAsString(), User.class).getName(), is(equalTo(t.getName())));
    }

    @Test
    public void updateUserTest() throws Exception {
        User t = new User(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        when(userService.save(any(User.class))).thenReturn(t);

        MvcResult result = mockMvc.perform(put("/users/{id}", t.getId())
                .content(mapper.writeValueAsString(t))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertThat(mapper.readValue(result.getResponse().getContentAsString(), User.class).getName(), is(equalTo(t.getName())));
    }

    @Test
    public void deleteUserTest() throws Exception {
        String id = UUID.randomUUID().toString();
        doNothing().when(userService).deleteById(id);

        MvcResult result = mockMvc.perform(delete("/users/{id}", id))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }
}
