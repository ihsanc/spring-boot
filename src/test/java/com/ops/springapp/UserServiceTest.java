package com.ops.springapp;

import com.ops.springapp.exception.EntityNotFoundException;
import com.ops.springapp.user.User;
import com.ops.springapp.user.UserRepository;
import com.ops.springapp.user.UserServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    UserServiceImpl userService;

    @Mock
    UserRepository userRepositoryMock;

    public static User generateNewUser() {
        return new User(UUID.randomUUID().toString(), UUID.randomUUID().toString());
    }

    @Before
    public void setUp() {
        userService = new UserServiceImpl();
        userService.setUserRepository(userRepositoryMock);
    }

    @Test
    public void userListTest() {
        List<User> userList = Arrays.asList(generateNewUser(), generateNewUser());
        when(userRepositoryMock.findAll()).thenReturn(userList);
        List<User> users = userService.getList();
        assertThat(users, is(equalTo(userList)));
    }

    @Test
    public void getUserByIdTest() {

        User expectedUser = generateNewUser();
        when(userRepositoryMock.findById(expectedUser.getId())).thenReturn(Optional.of(expectedUser));
        User actualUser = userService.getById(expectedUser.getId());
        assertThat(expectedUser, is(equalTo(actualUser)));


        when(userRepositoryMock.findById(expectedUser.getId())).thenReturn(Optional.empty());
        thrown.expect(EntityNotFoundException.class);
        thrown.expectMessage(contains(expectedUser.getId()));
        userService.getById(expectedUser.getId());
    }

    @Test
    public void deleteUserTest() {
        String id = UUID.randomUUID().toString();
        doNothing().when(userRepositoryMock).deleteById(id);
        userService.deleteById(id);
        verify(userRepositoryMock, times(1)).deleteById(id);
    }

    @Test
    public void saveUserTest() {
        User expectedUser = generateNewUser();
        when(userRepositoryMock.save(expectedUser)).thenReturn(expectedUser);
        User actualUser = userService.save(expectedUser);
        assertThat(expectedUser, is(equalTo(actualUser)));
    }
}
