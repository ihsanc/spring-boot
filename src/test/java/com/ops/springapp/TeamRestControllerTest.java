package com.ops.springapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ops.springapp.exception.EntityNotFoundException;
import com.ops.springapp.team.Team;
import com.ops.springapp.team.TeamRestController;
import com.ops.springapp.team.TeamServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Collections;
import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TeamRestController.class)
public class TeamRestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TeamServiceImpl teamService;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void listTeamTest() throws Exception {
        when(teamService.getList()).thenReturn(Collections.<Team>emptyList());

        mockMvc.perform(get("/teams"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));
    }

    @Test
    public void getTeamTest() throws Exception {
        Team t = new Team(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        when(teamService.getById(t.getId())).thenReturn(t);

        MvcResult result = mockMvc.perform(get("/teams/{id}", t.getId()))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        assertThat(mapper.readValue(result.getResponse().getContentAsString(), Team.class).getName(), is(equalTo(t.getName())));

        // Exception case
        when(teamService.getById(t.getId())).thenThrow(new EntityNotFoundException(Team.class, t.getId()));
        mockMvc.perform(get("/teams/{id}", t.getId()))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void addTeamTest() throws Exception {
        Team t = new Team(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        when(teamService.save(any(Team.class))).thenReturn(t);

        MvcResult result = mockMvc.perform(post("/teams")
                .content(mapper.writeValueAsString(t))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        assertThat(mapper.readValue(result.getResponse().getContentAsString(), Team.class).getName(), is(equalTo(t.getName())));
    }

    @Test
    public void updateTeamTest() throws Exception {
        Team t = new Team(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        when(teamService.save(any(Team.class))).thenReturn(t);

        MvcResult result = mockMvc.perform(put("/teams/{id}", t.getId())
                .content(mapper.writeValueAsString(t))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertThat(mapper.readValue(result.getResponse().getContentAsString(), Team.class).getName(), is(equalTo(t.getName())));
    }

    @Test
    public void deleteTeamTest() throws Exception {
        String id = UUID.randomUUID().toString();
        doNothing().when(teamService).deleteById(id);

        MvcResult result = mockMvc.perform(delete("/teams/{id}", id))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }
}
