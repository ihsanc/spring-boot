package com.ops.springapp;

import com.ops.springapp.exception.EntityNotFoundException;
import com.ops.springapp.team.Team;
import com.ops.springapp.team.TeamRepository;
import com.ops.springapp.team.TeamServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.*;

/**
 * @author ihsanc
 * @date 18.07.2018
 */
@RunWith(MockitoJUnitRunner.class)
public class TeamServiceTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    TeamServiceImpl teamService;

    @Mock
    TeamRepository teamRepositoryMock;

    public static Team generateNewTeam() {
        return new Team(UUID.randomUUID().toString(), UUID.randomUUID().toString());
    }

    @Before
    public void setUp() {
        teamService = new TeamServiceImpl();
        teamService.setTeamRepository(teamRepositoryMock);
    }

    @Test
    public void teamListTest() {
        List<Team> teamList = Arrays.asList(generateNewTeam(), generateNewTeam());
        when(teamRepositoryMock.findAll()).thenReturn(teamList);
        List<Team> teams = teamService.getList();
        assertThat(teams, is(equalTo(teamList)));
    }

    @Test
    public void getTeamByIdTest() {
        Team expectedTeam = generateNewTeam();
        when(teamRepositoryMock.findById(expectedTeam.getId())).thenReturn(Optional.of(expectedTeam));
        Team actualTeam = teamService.getById(expectedTeam.getId());
        assertThat(expectedTeam, is(equalTo(actualTeam)));


        when(teamRepositoryMock.findById(expectedTeam.getId())).thenReturn(Optional.empty());
        thrown.expect(EntityNotFoundException.class);
        thrown.expectMessage(contains(expectedTeam.getId()));
        teamService.getById(expectedTeam.getId());
    }

    @Test
    public void deleteTeamTest() {
        String id = UUID.randomUUID().toString();
        doNothing().when(teamRepositoryMock).deleteById(id);
        teamService.deleteById(id);
        verify(teamRepositoryMock, times(1)).deleteById(id);
    }

    @Test
    public void saveTeamTest() {
        Team expectedTeam = generateNewTeam();
        when(teamRepositoryMock.save(expectedTeam)).thenReturn(expectedTeam);
        Team actualTeam = teamService.save(expectedTeam);
        assertThat(expectedTeam, is(equalTo(actualTeam)));
    }
}
